from django.test import TestCase, Client
from django.http import HttpRequest
from pyvirtualdisplay import Display
from . import views
from .models import Status
from selenium import webdriver
import time
from datetime import datetime
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
class BoardUTestCase(TestCase):

	# cek apakah landing page ada
	def test_exist_landing(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_exist_landing_greeting(self):
		response = Client().get('')
		self.assertContains(response, "Halo, apa kabar?",
							count = 1, html=True)

	def test_model_saving_works(self):

		test_data = Status(text = "This is a test status")
		this_time = datetime.now()
		test_data.save()

		# cek apakah mode tersimpan
		self.assertEqual(Status.objects.count(), 1)
		self.assertIsInstance(test_data, Status)

		# cek apakah datetime tersimpan secara automatis
		saved_time = test_data.submitted_time
		self.assertEqual(this_time.year, saved_time.year)
		self.assertEqual(this_time.month, this_time.month)
		self.assertEqual(this_time.day, this_time.day)

	def test_status_form_exists(self):

		response = Client().get('')

		# hanya ada 1 form yang namanya 'status_input'
		self.assertContains(response, 'name="status_input"',
							count = 1, html = False)

	def test_form_submit_saves(self):

		response = Client().post('/save_post/', {'status_input' : 'this is just a test'}, follow = True)

		self.assertEqual(Status.objects.count(), 1)
		self.assertEqual(Status.objects.all()[0].text, "this is just a test")

	# tes apakah model hanya menyimpan status dengan panjang maks 300 karakter
	def test_form_saves_max_300_chars(self):

		long_text = ''
		for i in range(400):
			long_text+='g'

		response = Client().post('/save_post/', {'status_input' : long_text}, follow = True)

		self.assertEqual(Status.objects.count(), 0)

	def test_web_displays_no_data(self):

		response = Client().get('')
		self.assertContains(response, "Tidak ada status untuk saat ini", count=1, html=False)

	def test_web_displays_data(self):

		# melakukan posting 5 data
		Client().post('/save_post/', {'status_input' : 'this is test number 1'}, follow = True)
		Client().post('/save_post/', {'status_input' : 'this is test number 2'}, follow = True)
		Client().post('/save_post/', {'status_input' : 'this is test number 3'}, follow = True)
		Client().post('/save_post/', {'status_input' : 'this is test number 4'}, follow = True)
		Client().post('/save_post/', {'status_input' : 'this is test number 5'}, follow = True)

		# mengecek apakah setiap data di post
		response = Client().get('')
		self.assertContains(response, "this is test number 1", count=1, html=False)
		self.assertContains(response, "this is test number 2", count=1, html=False)
		self.assertContains(response, "this is test number 3", count=1, html=False)
		self.assertContains(response, "this is test number 4", count=1, html=False)
		self.assertContains(response, "this is test number 5", count=1, html=False)

# functional test dari website ini
class BoardFunctionalTest(TestCase):

	# set-up chrome minimalis
	def setUp(self):
		# global display
		self.display = Display(visible=0, size=(800, 600))
		self.display.start()
		# option yang didapat dari internet agar berhasil. 
		# source: https://gitlab.com/codefire53/story-6/blob/master/landing/tests.py
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		# chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36')
		service_log_path='./chromedriver.log'
		service_args=['--verbose']
		self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
		# self.browser.set_window_size(1024, 600)
		# self.browser.maximize_window()




	def tearDown(self):
		# global display

		self.display.stop()
		

		self.browser.quit()

	def test_input_successfull(self):
		

		response = self.browser.get('localhost:8000')
		time.sleep(5)
		print(self.browser.page_source)
		search_box = self.browser.find_element_by_name('status_input')
		search_box.send_keys('Coba coba')
		time.sleep(2)
		search_box.submit()

		self.assertIn("Coba coba", self.browser.page_source)

		time.sleep(2)


